import React from 'react';
import PropTypes from 'prop-types';

import './styles/image.scss';

const Image = ({
    altText, 
    styleName,
    imageUrl,
}) => (
    <img 
        alt={altText} 
        className={`image ${styleName}`} 
        src={imageUrl}
    />
);

Image.defaultProps = {
    altText: '',
    styleName: '',
    imageUrl: ''
};

Image.propTypes = {
    altText: PropTypes.string,
    styleName: PropTypes.string,
    imageUrl: PropTypes.string
};

export default Image;