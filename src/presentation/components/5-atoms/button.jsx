import React from 'react';
import PropTypes from 'prop-types';

import './styles/button.scss';

const Button = ({
    text, 
    onClickFunc,
    styleName
}) => {
    return (
        <button className={`button${styleName ? ` button--${styleName}` : ''}`} onClick={onClickFunc}>
            {text}
        </button>
    );
};

Button.defaultProps = {
    text: '',
    onClickFunc: undefined,
    styleName: '',
};

Button.propTypes = {
    text: PropTypes.string,
    onClickFunc: PropTypes.func,
    styleName: PropTypes.string,
};

export default Button;