import React from 'react';
import PropTypes from 'prop-types';

import './styles/paragraph.scss';

const Paragraph = ({
    text,
    customClass, 
}) => {
    return (
        <p className={`paragraph ${customClass}`}>
            {text}
        </p>
    );
};

Paragraph.defaultProps = {
    text: '',
    customClass: ''
};

Paragraph.propTypes = {
    text: PropTypes.string,
    customClass: PropTypes.string,
};


export default Paragraph;