import React from 'react';
import PropTypes from 'prop-types';

import './styles/heading.scss';

const Heading = ({
    text, 
    styleName
}) => {
    return (
        <h1 className={`heading${styleName ? ` heading--${styleName}` : ''}`}>
            {text}
        </h1>
    );
};

Heading.defaultProps = {
    text: '',
    styleName: ''
};

Heading.propTypes = {
    text: PropTypes.string,
    styleName: PropTypes.string
};


export default Heading;