import { connect } from 'react-redux';
import GrazeFeed from './grazeFeed.jsx';
import { postsFetchData } from '../../../actions/getPostsAction';

const mapStateToProps = (state) => {
    return {
        posts: state.posts,
        hasErrored: state.postsHasErrored,
        isLoading: state.postsIsLoading,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(postsFetchData(url))
    };
};


export default connect(
    mapStateToProps, 
    mapDispatchToProps)(GrazeFeed);