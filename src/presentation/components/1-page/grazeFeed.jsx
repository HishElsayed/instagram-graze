import React, { Component } from 'react';
import Paragraph from '../5-atoms/paragraph.jsx';
import Heading from '../5-atoms/heading.jsx';
import Carousel from '../3-organisms/carousel.jsx';

import './styles/graze-feed.scss';
class GrazeFeed extends Component {

    constructor(props) {
        super(props);
    }
    
    componentDidMount() {
        this.props.fetchData('https://graze-coding-test-api.herokuapp.com/carousel');
    }
    render() {
        
        if (this.props.hasErrored) {
            
            return (
                <div className="container">
                    <Heading styleName="large" text="404 - no snacks here!" />
                    <Paragraph text="We can't find the page you're looking for. Try double checking the address that you've entered. If you need any help please get in touch." />
                </div>
            );
        }

        if (this.props.isLoading || this.props.posts.length === 0) {
            return (
                <div className="container">
                    <svg className='container__loader' viewBox='25 25 50 50'>
                        <circle className='container__loader-path' cx='50' cy='50' r='20' fill='none' stroke='#00989e'
                            strokeWidth='2' />
                    </svg>
                </div>
            );
        }

        const postData = this.props.posts.data;
        const paginationData = this.props.posts.pagination;
        return (
            <div className="container">
                <Heading text="our grazers' testimonials" />
                <Paragraph text="millions have tried &#38; loved their #grazesnacks" />
                <Carousel postData={postData} />    
            </div>
        );
    }
}

export default GrazeFeed;