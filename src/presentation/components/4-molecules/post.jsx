import React from 'react';
import PropTypes from 'prop-types';
import Paragraph from '../5-atoms/paragraph.jsx';
import Heading from '../5-atoms/heading.jsx';
import Image from '../5-atoms/image.jsx';

import './styles/post.scss';

const Post = (postData) => {
    return (
        <div className="posts">
            {postData.postData.postData.map(data => (
                <article className="posts__post" key={data.id}>
                    <section className="posts__image-container">
                        <Image imageUrl={data.post.image} />
                    </section>
                    <section className="posts__comment-container">
                        <div className="posts__username-container">
                            <Image styleName="posts__profile-picture" imageUrl={data.user.profile_picture} />
                            <h3 className="posts__username">
                                {data.user.username}
                            </h3>
                        </div>
                        <Paragraph customClass="posts__paragraph" text={data.post.text} />
                    </section>
                </article>
            ))}          
        </div>    
    );
};

Post.defaultProps = {
    text: '',
};

Post.propTypes = {
    text: PropTypes.string,
};

export default Post;