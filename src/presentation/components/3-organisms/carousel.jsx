import React from 'react';
import PropTypes from 'prop-types';
import Post from '../4-molecules/post.jsx';
import Button from '../5-atoms/button.jsx';


import './styles/carousel.scss';

const Carousel = (postData) => {
    return (
        <div className="carousel">
            <Button styleName="left-arrow" />
            <Post postData={postData} />
            <Button styleName="right-arrow" />
        </div>    
    );
};

Carousel.defaultProps = {
    propTypes: [],
};

Carousel.propTypes = {
    postData: PropTypes.array,
};

export default Carousel;