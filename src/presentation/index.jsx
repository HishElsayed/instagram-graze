import { AppContainer } from 'react-hot-loader';
import React from 'react';  
import { render } from 'react-dom';  
import { Provider } from 'react-redux';
import configureStore from '../store/configureStore';
import GrazeFeed from './components/1-page/grazeFeedContainer.jsx';

const store = configureStore();

renderWithHotReload(GrazeFeed);

if (module.hot) {
    module.hot.accept('./components/1-page/grazeFeedContainer.jsx', () => {
        const GrazeFeed = require('./components/1-page/grazeFeedContainer.jsx').default;
        renderWithHotReload(GrazeFeed);
    });
}

function renderWithHotReload(GrazeFeed) {
    render ( 
        <Provider store={store}>
            <GrazeFeed />
        </Provider>, 
        document.getElementById('app')
    );
}