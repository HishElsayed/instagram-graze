export function postsIsLoading(state = false, action) {
    switch (action.type) {
    case 'POSTS_IS_LOADING':
        return action.isLoading;

    default:
        return state;
    }
}