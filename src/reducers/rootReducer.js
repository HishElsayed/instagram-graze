import { combineReducers } from 'redux';
import { postsHasErrored } from './postsErroredReducer';
import { postsIsLoading } from './postsLoadingReducer';
import { posts } from './postsFetchedReducer';

export default combineReducers({
    posts,
    postsHasErrored,
    postsIsLoading
});