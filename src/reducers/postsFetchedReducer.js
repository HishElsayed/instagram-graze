export function posts(state = [], action) {
    switch (action.type) {
    case 'POSTS_FETCH_DATA_SUCCESS':
        return action.posts;

    default:
        return state;
    }
}
