const commonPaths = require("./common-paths");
const config = {
    target: 'web',
    entry: {
        'bundle': './src/presentation/index.jsx'
    },
    entry: {
        polyfills: [
            'core-js/es6/set'
        ],
        bundle: './src/presentation/index.jsx'

    },
    output: {
        filename: 'static/js/[name].js',
        path: commonPaths.outputPath,

        // There are also additional JS chunk files if you use code splitting.
        chunkFilename: 'static/js/[name].chunk.js',

        // This is the URL that app is served from. We use "/" in development.
        publicPath: '/'
    }
};

module.exports = config;
